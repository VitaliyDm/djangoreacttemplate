import React from 'react'
import ReactDOM from 'react-dom'


function Welcome({name}) {
  console.log(name)
  return <div>React {name}</div>;
}

window.render_my_thing = function(properties) {
    const element = <Welcome name={properties.name} />;
    const mountElem = document.createElement('div');

    mountElem.setAttribute('id', 'react')

    // document.appendChild(mountElem)
    ReactDOM.render(
      element,
      document.getElementById('react')
    );
}