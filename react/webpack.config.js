var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');

module.exports = {
  context: __dirname,

  entry: './static/js/index',

  output: {
      path: path.resolve('../myproject/todo/static/bundles/'),
      filename: "[name].js",
  },
  mode: 'production',

  plugins: [
    new BundleTracker({filename: './static/webpack-stats.json'}),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  }

};
