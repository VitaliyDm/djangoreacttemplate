from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add', views.add, name='add'),
    path('<int:pk>/edit', views.edit, name='edit'),
    path('<int:pk>/done', views.done, name='done'),
]
