from django.urls import path
from . import view_classes

urlpatterns = [
    path('', view_classes.Index.as_view(), name='index'),
    path('add', view_classes.Add.as_view(), name='add'),
    path('<int:pk>/edit', view_classes.Edit.as_view(), name='edit'),
    path('<int:pk>/done', view_classes.Done.as_view(), name='done'),
]
