from django.db import models


class Todo(models.Model):
    name = models.CharField("name", max_length=50)
    done = models.BooleanField("Done")
    created_at = models.DateTimeField("Creation date and time", auto_now_add=True)

    def __str__(self):
        return self.name
