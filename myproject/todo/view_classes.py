from django.shortcuts import redirect
from django.views import generic
from django.views.generic.detail import SingleObjectMixin
from django.core.urlresolvers import reverse_lazy
from .models import Todo
from .forms import TodoForm


class Index(generic.ListView):
    template_name = 'index.html'

    def get_queryset(self):
        if self.request.GET.get('all') == '1':
            queryset = Todo.objects.all()
        else:
            queryset = Todo.objects.filter(done=False)
        todo_list = queryset.order_by('-created_at')
        return todo_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['todo_list'] = context['object_list']
        return context


class Add(generic.CreateView):
    form_class = TodoForm
    template_name = 'add.html'
    success_url = reverse_lazy('index')


class Edit(generic.UpdateView):
    model = Todo
    form_class = TodoForm
    template_name = 'edit.html'
    success_url = reverse_lazy('index')


class Done(SingleObjectMixin, generic.View):
    model = Todo

    def get(self, request, *args, **kwargs):
        todo = self.get_object()
        todo.done = True
        todo.save()
        return redirect('index')
